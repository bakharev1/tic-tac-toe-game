import Game from "./components/Game";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";


ReactDOM.render(
    <Game />,
    document.getElementById("root")
);
